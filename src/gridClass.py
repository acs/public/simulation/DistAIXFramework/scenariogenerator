###############################################################################
# Grid class module
#
# @author Sonja Happ <sonja.happ@eonerc.rwth-aachen.de>, Felix Wege
# @copyright 2019, Institute for Automation of Complex Power Systems, EONERC
# @license GNU General Public License (version 3)
#
# DistAIX Scenario Generator
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#############################################################################

# Class that is used to store metadata for the grids
class Grid:
    def __init__(self, path, pathComponents, pathElectricalGrid, voltageLevel, highestNodeId, highestComponentId, lvIndex = None, pathWishedConnections = None):
        self.path = path
        self.pathComponents = pathComponents
        self.pathElectricalGrid = pathElectricalGrid
        self.voltageLevel = voltageLevel
        self.highestNodeId = highestNodeId
        self.highestComponentId = highestComponentId
        self.lvIndex = lvIndex
        self.pathWishedConnections = pathWishedConnections