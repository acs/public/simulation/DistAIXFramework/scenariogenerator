###############################################################################
# Scenario Generator execution file
#
# @author Sonja Happ <sonja.happ@eonerc.rwth-aachen.de>, Felix Wege
# @copyright 2019, Institute for Automation of Complex Power Systems, EONERC
# @license GNU General Public License (version 3)
#
# DistAIX Scenario Generator
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#############################################################################


# This is a modular ScenarioGenerator.
# It can be used to create new grid topologies and add consumer and producer components to the given nodes.
# Also, given low voltage grids can be attached to random unoccupied nodes of medium voltage grids.
# The different 
# Additional arguments: 
#                       '-lv'       --> only low voltage grid creation is activated (Create components in low voltage grids)
#                       '-mv'       --> only medium voltage grid creation is activated (Create components in medium voltage grids)
#                       '-full'     --> full grid creation activated (Merging LV/MV grids to a full grid)
#                       '-top'      --> topology creation activated (Create new topologies from scratch)
#                       '-plot'     --> the resulting new scenario is additionally saved as a .png plot
# !! CARE !! Plot function only usable for small grids!
# To be able to use the plot feature, pydot has to be installed.
# If no pydot is installed, simply call the script without '-plot' option, and the script will not try to import the corresponding packages.
#
# Due to the use of the pathlib module, at least Python 3.4.0 has to be used.
# Python 3.6 is recommended!
#
# #############################################################################################
# Requirements for subtype files:
# 1) There has to be one '*_mv' ,as well as, one '*_lv' file for each component type
# 2) Files have to be in ascending order relating to the nominal power S_r[VA]
# 3) The nominal power S_r[VA] has to be in the fifth column (--> index = 4) !
#
# Requirements for components.csv file:
# For all components: id,type,*
# For Nodes: id,type,trafoID,*
# * --> don't care
#
# Requirements for el_grid.csv file:
# id1,id2,cabletype,arg1*,arg2*,arg3*,arg4*,arg5*,arg6*,arg7*
# * --> not used and therefore can have arbitrary order/content
#
# Requirements for lv_grids.csv file:
# Each row includes the relative path of the directories of the wished low voltage grids.
# By default, '../' is added to each path!
#
# Requirements for subtypes_perc.csv file:
# Each row has to start with the identifier of the wished component type 'load_lv / chp_mv / ...'
# Followed by that, a percentage value [0.0,1.0] for each possible subtype has to follow in the same order,
# the subtypes are arranged in the corresponding subtypes file.
# !! It is possible NOT to give percentages for a component type, then the generator distributes the nominal power equally !!

import datetime
import sys

# import local modules
import scenarioGenerator
import csvReaderWriter

# Variable to store the relative path to the newScenario Dictionary
dateTime = datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
newScenarioPath = ''.join(['../generatedgrids/',dateTime])

# Check, if additional arguments were passed
argvList = list()

plotGraph = False
onlyLV = False
onlyMV = False
fullGridRandom = False
fullGridFixed = False
topology = False
autoTopology = False
mergeScenarios = False

# Read in additional arguments
if len(sys.argv) > 1:
    for args in range(1,len(sys.argv)):
        argvList.append(sys.argv[args])
    if '-plot' in argvList:
        plotGraph = True
        import plotter
    if '-lv' in argvList:
        onlyLV = True
    if '-mv' in argvList:
        onlyMV = True
    if '-full-random' in argvList:
        fullGridRandom = True
    if '-full-fixed' in argvList:
        fullGridFixed = True
    if '-top' in argvList:
        if '-auto' in argvList:
            autoTopology = True
        else:
            topology = True
    if '-merge-scenarios' in argvList:
        mergeScenarios = True
            
# Determine what to do, based on given arguments
if onlyLV:
    # Add components to given low voltage grid
    scenarioGenerator.ComponentGridCreation('lv',newScenarioPath)
elif onlyMV:
    # Add components to given medium voltage grid
    scenarioGenerator.ComponentGridCreation('mv',newScenarioPath)
elif fullGridRandom:
    # Attach low voltage grids to given medium voltage grid randomly
    scenarioGenerator.fullGridCreationRandom(newScenarioPath)
elif fullGridFixed:
    # Attach low voltage grids to fixed nodes in medium voltage grid
    scenarioGenerator.fullGridCreationFixed(newScenarioPath)
elif topology:
    # Create new topology
    voltageLevel = input('Voltage level? [mv/lv]:')
    scenarioGenerator.recTopologyCreation('feeder','0', voltageLevel,1,1)
elif autoTopology:
    # Automated creation of topologies from a given config file
    autoGenerationGridsDct = {}
    filePath = input('Path configuration file: ')
    csvReaderWriter.readAutoGenerationGrids(autoGenerationGridsDct,filePath)

    #print(autoGenerationGridsDct)
    datePath = newScenarioPath
    for grid in autoGenerationGridsDct:

        newScenarioPath = '_'.join([datePath,grid])

        # The dicts storing components and electrical connections have to be cleared to remove the content of previous runs
        scenarioGenerator.newScenarioComponents.clear()
        scenarioGenerator.newScenarioElectricalGrid.clear()
        scenarioGenerator.componentsDct.clear()
        scenarioGenerator.electricalgridsDct.clear()

        scenarioGenerator.autoTopologyCreation(autoGenerationGridsDct[grid][0],int(autoGenerationGridsDct[grid][1]), 
        int(autoGenerationGridsDct[grid][2]),autoGenerationGridsDct[grid][3],autoGenerationGridsDct[grid][4],float(autoGenerationGridsDct[grid][5]))

        scenarioGenerator.newScenarioComponents.sort(key=lambda k: k[0])
        scenarioGenerator.newScenarioElectricalGrid.sort(key=lambda k: k['id1'])

        # Write Final files!!
        csvReaderWriter.writeGrid(newScenarioPath, scenarioGenerator.newScenarioComponents, scenarioGenerator.newScenarioElectricalGrid)

        scenarioGenerator.autoComponentGridCreation(autoGenerationGridsDct[grid][0],newScenarioPath,newScenarioPath)
        
        scenarioGenerator.newScenarioComponents.sort(key=lambda k: k[0])
        scenarioGenerator.newScenarioElectricalGrid.sort(key=lambda k: k['id1'])

        # Write Final files!!
        csvReaderWriter.writeGrid(newScenarioPath, scenarioGenerator.newScenarioComponents, scenarioGenerator.newScenarioElectricalGrid)

        # If plotting is activated, call plotter module to create the plot
        if plotGraph:
            plotter.plot(scenarioGenerator.scenariosList, scenarioGenerator.newScenarioComponents, scenarioGenerator.newScenarioElectricalGrid, newScenarioPath)
elif mergeScenarios:
    # merge two grids
    scenarioGenerator.mergeScenarios(newScenarioPath)
else:
    print('Nothing to do...')

if not autoTopology:
    # Sort components and edges, to be sure the components are given in ascending order
    scenarioGenerator.newScenarioComponents.sort(key=lambda k: k[0])
    scenarioGenerator.newScenarioElectricalGrid.sort(key=lambda k: k['id1'])

    # Write Final files!!
    csvReaderWriter.writeGrid(newScenarioPath, scenarioGenerator.newScenarioComponents, scenarioGenerator.newScenarioElectricalGrid)

    # If plotting is activated, call plotter module to create the plot
    if plotGraph:
        plotter.plot(scenarioGenerator.scenariosList, scenarioGenerator.newScenarioComponents, scenarioGenerator.newScenarioElectricalGrid, newScenarioPath)