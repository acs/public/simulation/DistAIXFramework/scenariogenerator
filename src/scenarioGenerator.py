###############################################################################
# Scenario Generator module
#
# @author Sonja Happ <sonja.happ@eonerc.rwth-aachen.de>, Felix Wege
# @copyright 2019, Institute for Automation of Complex Power Systems, EONERC
# @license GNU General Public License (version 3)
#
# DistAIX Scenario Generator
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
###############################################################################

# This module contains the core functions, realizing the different functionalities of the ScenarioGenerator
import random
import copy
from pathlib import Path

# Import local modules
import componentCreator
import inputReader
import csvReaderWriter
import helperFunctions
import gridClass

# empty lists to save scenario properties as well as the components and cables
scenariosList = list()
newScenarioComponents = list()
newScenarioElectricalGrid = list()
connectionNodes = list()
LVconnectionNodes = list()

isConnectionNode = list()
nodeIsOccupied = list()
newComponentsList = list()
newElectricalgridsList = list()
# structure [id of the connected node, id of the component, type of component]
# list should help to keep the rules
handleComponents = list()

# empty dicts to store components, cables and the number of outgoing edges
connectionsDct = {}
componentsDct = {}
electricalgridsDct = {}
edgecountDct = {}
numberOfComponentsDct = {}
wishedKWperComponentDct = {}
actualKWperComponentDct = {}
communicationPercDct = {}
edgecountDct1 = {}
edgecountDct2 = {}

# for the deleted components and grids
tempComponentsDct = []
tempElectricalgridsDct = []

# dict for change the ids to sort the list
newIdsDct = {}

# Function to attach the wished components to a given grid
def ComponentGridCreation(voltageLevel, newScenarioPath):
    # Determine if low or medium voltage component creation is wished
    if voltageLevel == 'lv':
        print('Low voltage grid creation started...\n')
        while True:
            pathGrid = '/'.join(('..', input('Low voltage grid: ')))
            # Check if path is valid and all required files are present
            if Path(pathGrid).is_dir():
                if Path('/'.join([pathGrid, 'components.csv'])).is_file():
                    if Path('/'.join([pathGrid, 'el_grid.csv'])).is_file():
                        if Path('/'.join([pathGrid, 'subtypes_perc.csv'])).is_file():
                            break
                        else:
                            print('No subtypes_perc.csv file found...')
                    else:
                        print('No el_grid.csv file found...')
                else:
                    print('No components.csv file found...')
            else:
                print('\'%s\' is no directory - try again!' % pathGrid)


    elif voltageLevel == 'mv':
        print('Medium voltage grid creation started..\n')
        while True:
            pathGrid = '/'.join(('..', input('Medium voltage grid: ')))
            # Check if path is valid and all required files are present
            if Path(pathGrid).is_dir():
                if Path('/'.join([pathGrid, 'components.csv'])).is_file():
                    if Path('/'.join([pathGrid, 'el_grid.csv'])).is_file():
                        if Path('/'.join([pathGrid, 'subtypes_perc.csv'])).is_file():
                            break
                        else:
                            print('No subtypes_perc.csv file found...')
                    else:
                        print('No el_grid.csv file found...')
                else:
                    print('No components.csv file found...')
            else:
                print('\'%s\' is no directory - try again!' % pathGrid)

    else:
        print('%s is not a valid voltagelevel!' % voltageLevel)
        return

    scenariosList.append(gridClass.Grid(path=pathGrid, pathComponents='/'.join((pathGrid, 'components.csv')),
                                        pathElectricalGrid='/'.join((pathGrid, 'el_grid.csv')),
                                        voltageLevel=voltageLevel,
                                        highestNodeId=0, highestComponentId=0))

    componentsDct['_'.join(('components', voltageLevel))] = []
    electricalgridsDct['_'.join(('elecgrid', voltageLevel))] = []

    # Read in all scenarios
    for scenario in scenariosList:
        csvReaderWriter.readGrid(scenario, componentsDct, electricalgridsDct)

    # Find all 'free nodes'
    helperFunctions.determineConnectionNodes(scenariosList[0], 3, edgecountDct, connectionNodes, electricalgridsDct)

    # If user wishes the creation of MV components, fill the corresponding dictionary
    if input('Component creation wished? [y/n]') is 'y':
        # Read in wished nominal powers from file or from user input
        if input('Read wished nominal powers from file? [y/n] ') is 'y':
            if Path('/'.join([pathGrid, 'wished_nominal_power.csv'])).is_file():
                # Read in nominal powers from file
                csvReaderWriter.readNominalPowerFromCSV(voltageLevel, wishedKWperComponentDct, pathGrid)
            else:
                print('No wished_nominal_power.csv file found... Manual input routine executed...')
                inputReader.readNominalPower(voltageLevel, wishedKWperComponentDct)
        else:
            # Read in user specified nominal powers for all components
            inputReader.readNominalPower(voltageLevel, wishedKWperComponentDct)

    # Adding flag to node to enable/disable participation in communication
    if input(
            'Adding new communication flag feature? This will add another column for nodes! Currently not compatible with Simulator!...[y/n]') == 'y':
        # helperFunctions.addCommunicationPerc(scenariosList[0],componentsDct)
        helperFunctions.addCommunicationPercNew(scenariosList[0], componentsDct, edgecountDct)
        inputReader.readCommunicationPerc(voltageLevel, communicationPercDct)

    # Read in subtypes
    subTypesDct = {}
    subTypesListsDct = {}
    subTypesPercDct = {}

    # Read in possible subtypes and the user wished percentages
    csvReaderWriter.readComponentSubTypes(subTypesDct, False)
    csvReaderWriter.readSubtTypesPercentages(scenariosList[0], subTypesPercDct)
    # pdb.set_trace()
    # Determine the used Subtypes
    componentCreator.determineSubtypes(voltageLevel, wishedKWperComponentDct, actualKWperComponentDct,
                                       numberOfComponentsDct, subTypesDct, subTypesListsDct, subTypesPercDct)

    # Additional lists required to make sure only HP or CHP per node and Batteries only with producers
    hpChp = list()
    producerComponents = list()

    # For each component type, check if the given number is greater than 0.
    # For each type greater than 0, call componentCreater to create all components of this type
    for key in numberOfComponentsDct:
        if numberOfComponentsDct[key] > 0:
            # pdb.set_trace()
            componentCreator.create(voltageLevel, True, actualKWperComponentDct, numberOfComponentsDct[key], key,
                                    connectionNodes, hpChp, producerComponents,
                                    0, scenariosList, componentsDct, electricalgridsDct, subTypesListsDct,
                                    newScenarioComponents, newScenarioElectricalGrid, communicationPercDct)

    # Clear dictionary after use for later use in LV component creation
    numberOfComponentsDct.clear()

    # Write csv file including actual nominal powers
    csvReaderWriter.writeNominalPower(newScenarioPath, actualKWperComponentDct)

    # Create the actual grid
    for row in componentsDct['_'.join(['components', voltageLevel])]:
        print(str(row[0]) + row[1])
        # If node found
        newScenarioComponents.append(row)

    for row in electricalgridsDct['_'.join(['elecgrid', voltageLevel])]:
        newScenarioElectricalGrid.append(row)

# Function for automated component creation in simple topologies for scalability analysis
def autoComponentGridCreation(voltageLevel, newScenarioPath, pathGrid):
    scenariosList.clear()
    newScenarioComponents.clear()
    newScenarioElectricalGrid.clear()

    scenariosList.append(gridClass.Grid(path=pathGrid, pathComponents='/'.join((pathGrid, 'components.csv')),
                                        pathElectricalGrid='/'.join((pathGrid, 'el_grid.csv')),
                                        voltageLevel=voltageLevel,
                                        highestNodeId=0, highestComponentId=0))

    componentsDct['_'.join(('components', voltageLevel))] = []
    electricalgridsDct['_'.join(('elecgrid', voltageLevel))] = []

    # Read in all scenarios
    for scenario in scenariosList:
        csvReaderWriter.readGrid(scenario, componentsDct, electricalgridsDct)

    # Find all 'free nodes'
    helperFunctions.determineConnectionNodes(scenariosList[0], 3, edgecountDct, connectionNodes, electricalgridsDct)

    # Read in possible subtypes and the user wished percentages
    subTypesDct = {}
    csvReaderWriter.readComponentSubTypes(subTypesDct, True)
    # csvReaderWriter.readSubtTypesPercentages(scenariosList[0],subTypesPercDct)

    # print(subTypesDct)

    if voltageLevel == 'mv':

        print('Wished number of kilowatts per medium voltage component reqiured...')

        subTypes = ['load', 'pv', 'chp', 'hp', 'compensator', 'biofuel', 'wind', 'storage']
        for subType in subTypes:
            if subType == 'load':
                wishedKWperComponentDct['_'.join([subType, 'mv'])] = float(scenariosList[0].highestNodeId - 3) * float(
                    subTypesDct['_'.join([subType, 'mv'])][0][4]) / 1000.0
            else:
                wishedKWperComponentDct['_'.join([subType, 'mv'])] = 0.0


    # Additional components for low voltage grids
    elif voltageLevel == 'lv':

        print('Wished number of kilowatts per low component reqiured...')

        subTypes = ['load', 'pv', 'chp', 'hp', 'compensator', 'ev', 'wind', 'storage']
        for subType in subTypes:
            if subType == 'load':
                wishedKWperComponentDct['_'.join([subType, 'lv'])] = float(scenariosList[0].highestNodeId - 3) * float(
                    subTypesDct['_'.join([subType, 'lv'])][0][4]) / 1000.0
            else:
                wishedKWperComponentDct['_'.join([subType, 'lv'])] = 0.0

    # # Adding flag to node to enable/disable participation in communication
    # if input('Adding new communication flag feature? This will add another column for nodes! Currently not compatible with Simulator!...[y/n]') == 'y':
    #     #helperFunctions.addCommunicationPerc(scenariosList[0],componentsDct)
    #     helperFunctions.addCommunicationPercNew(scenariosList[0], componentsDct, edgecountDct)
    #     inputReader.readCommunicationPerc(voltageLevel,communicationPercDct)

    # Read in subtypes
    subTypesListsDct = {}
    subTypesPercDct = {}

    # Determine the used Subtypes
    componentCreator.determineSubtypes(voltageLevel, wishedKWperComponentDct, actualKWperComponentDct,
                                       numberOfComponentsDct, subTypesDct, subTypesListsDct, subTypesPercDct)

    # Additional lists required to make sure only HP or CHP per node and Batteries only with producers
    hpChp = list()
    producerComponents = list()

    # For each component type, check if the given number is greater than 0.
    # For each type greater than 0, call componentCreater to create all components of this type
    for key in numberOfComponentsDct:
        if numberOfComponentsDct[key] > 0:
            componentCreator.create(voltageLevel, True, actualKWperComponentDct, numberOfComponentsDct[key], key,
                                    connectionNodes, hpChp, producerComponents,
                                    0, scenariosList, componentsDct, electricalgridsDct, subTypesListsDct,
                                    newScenarioComponents, newScenarioElectricalGrid, communicationPercDct)

    # Clear dictionary after use for later use in LV component creation
    numberOfComponentsDct.clear()

    # Write csv file including actual nominal powers
    csvReaderWriter.writeNominalPower(newScenarioPath, actualKWperComponentDct)

    # Create the actual grid
    for row in componentsDct['_'.join(['components', voltageLevel])]:
        # print(str(row[0]) + row[1])
        # If node found
        newScenarioComponents.append(row)

    for row in electricalgridsDct['_'.join(['elecgrid', voltageLevel])]:
        newScenarioElectricalGrid.append(row)


# Function to attach the low voltage grid at specified nodes at the chosen medium voltage grid (author: Kaying Lee)
def fullGridCreationFixed(newScenarioPath):

    print('connect LV grid to a fix MV node...\n')

    # if wished_connections.csv file is available ask user whether to use the file
    # if yes, then continue with the function
    # if no, then break and use the alternative function fullGridCreationRandom()
    readFile = str(input('read wished_connections.csv?[y/n]'))
    if readFile != 'y':
        # continue with fullGridCreation()
        print('use fullGridCreation() instead...')
        fullGridCreationRandom(newScenarioPath)
        return

    # Medium voltage grid is read and stored. The dcts for storing the corresponding components and cables are initialized
    pathMVGrid = '/'.join(('..', input('Medium voltage grid: ')))
    scenariosList.append(gridClass.Grid(path=pathMVGrid, pathComponents='/'.join((pathMVGrid, 'components.csv')),
                                        pathElectricalGrid='/'.join((pathMVGrid, 'el_grid.csv')), voltageLevel='mv',
                                        highestNodeId=0, highestComponentId=0))

    componentsDct['components_mv'] = []
    electricalgridsDct['elecgrid_mv'] = []

    # Read in mv grid, to be able to determine the number of free nodes
    csvReaderWriter.readGrid(scenariosList[0], componentsDct, electricalgridsDct)

    # Dict that later on stores the amount of occurences for each individual lv grid
    quantityOfLVGridsDct = {}

    # Dict that holds all possible lv grids
    lvGridList = list()

    csvReaderWriter.readLVGrids(pathMVGrid, lvGridList)

    # Process each possible lv grid...
    # we need components.csv, el_grid.csv and wished_connection.csv
    for grid in lvGridList:
        # initialize number occurrences with 0
        # maybe not using this list, because it counts the times of using the lv grids
        quantityOfLVGridsDct[grid] = 0
        pathLVGrid = '/'.join(('..', grid))

        # get data of lv-grids
#        scenariosList.append(
#            gridClassOverload.GridOverLoad(path=pathLVGrid, pathComponents='/'.join((pathLVGrid, 'components.csv')),
#                                           pathElectricalGrid='/'.join((pathLVGrid, 'el_grid.csv')),
#                                           pathWishedConnections='/'.join((pathLVGrid, 'wished_connections.csv')),
#                                           voltageLevel='lv', highestNodeId=0, highestComponentId=0,
#                                           lvIndex=len(scenariosList)))

        scenariosList.append(
            gridClass.Grid(path=pathLVGrid, pathComponents='/'.join((pathLVGrid, 'components.csv')),
                                           pathElectricalGrid='/'.join((pathLVGrid, 'el_grid.csv')),
                                           voltageLevel='lv', highestNodeId=0, highestComponentId=0,
                                           lvIndex=len(scenariosList),
                                           pathWishedConnections='/'.join((pathLVGrid, 'wished_connections.csv'))))

        # add empty list to store components of lv to the list
        # the list is already contain components of mv grid
        componentsDct['_'.join(['components_lv', str(len(scenariosList) - 1)])] = []
        # same with electrical grid
        electricalgridsDct['_'.join(['elecgrid_lv', str(len(scenariosList) - 1)])] = []
        # initialize the list for the connection edges between mv and lv
        # maybe not using
        connectionsDct['_'.join(['wishedConnections_mv_lv', str(len(scenariosList) - 1)])] = []

    m = 1
    for scenario in scenariosList[1:]:
        csvReaderWriter.readGridExtension(scenario, componentsDct, electricalgridsDct, connectionsDct, m)
        m = m + 1
    # number to expands the componentsDct, electricalgridsDct and connectionsDct and also to get the total number of grids that have to be created
    countGrids = 0
    for i in componentsDct:
        countGrids += 1
    print('number of grids: ')
    print(countGrids)
    # check if there are more than 1 entry in connectionsDct['wishedConnections_mv_lv_x']
    for z in range(1, countGrids):
        counter = 0
        temp = copy.deepcopy(connectionsDct['_'.join(['wishedConnections_mv_lv', str(z)])])
        for iterate in temp:
            tempNode = int(iterate[1])
            for search in connectionsDct['_'.join(['wishedConnections_mv_lv', str(z)])]:
                if tempNode == int(search[1]):
                    counter += 1
            if counter > 1:
                print('there are more than 1 connection entry: duplicate lv-grid')
                connectionsDct['_'.join(['wishedConnections_mv_lv', str(countGrids)])] = [iterate]
                connectionsDct['_'.join(['wishedConnections_mv_lv', str(z)])].remove(iterate)
                componentsDct['_'.join(['components_lv',str(countGrids)])] = copy.deepcopy(componentsDct['_'.join(['components_lv',str(z)])])
                electricalgridsDct['_'.join(['elecgrid_lv', str(countGrids)])] = copy.deepcopy(electricalgridsDct['_'.join(['elecgrid_lv', str(z)])])
                countGrids += 1
                print('number of grids: ')
                print(countGrids)
            counter = 0

    # verify lv-nodes in connectionsDct - it has to be a transformer node
    helperFunctions.checkLvConnection(connectionsDct,componentsDct, countGrids)
    print('transformer are given')
    # verify mv-nodes in connectionsDct - it has to be a type of node
    helperFunctions.checkMvConnection(scenariosList[0], connectionsDct, componentsDct)
    print('nodes in mv are given')
    # check if there are more than one connection with a node in mv-grid
    helperFunctions.checkUniqueness(scenariosList[0], connectionsDct, componentsDct)
    print('every mv-node is only connecting to one lv-grid')

    # procedure: take mv-grid and the first lv-grid of lv_grids.csv and read the respective wished_connections.csv
    # get content of wished_connections.csv of the respective lv_grid
    newConnectionsDct = {}
    # attention: range starts by 1 but we have to consider one grid lower because of mv-grid
    for iter2 in range(1, countGrids):
        #pdb.set_trace()
        newConnectionsDct['_'.join(['connection', str(iter2)])] = []
        for row in connectionsDct['_'.join(['wishedConnections_mv_lv', str(iter2)])]:
            # first entry of the row
            id_mv = int(row[0])
            # second entry of the row
            id_lv = int(row[1])
            # search the nodes in the respective components
            #pdb.set_trace()
            for mvRow in componentsDct['_'.join(['components', 'mv'])]:
                if mvRow[1] == 'Node':
                    # search for the matching node
                    if int(mvRow[0]) == id_mv:
                        # check if node is already occupied by another lv
                        if id_mv in nodeIsOccupied:
                            print('ATTENTION: node in mv-grid is already occupied by another lv-grid!!!')
                            return
                        else:
                            # check if node is free
                            helperFunctions.determineFreeNode(scenariosList[0], 3, id_mv, edgecountDct1, edgecountDct2,
                                                              isConnectionNode, electricalgridsDct)
                            # node is not free
                            if id_mv not in isConnectionNode:
                                freeNode = id_mv
                                # node is not free -> make it free
                                helperFunctions.getFreeNode(scenariosList[0], freeNode, tempComponentsDct,
                                                             tempElectricalgridsDct, electricalgridsDct, componentsDct)
                                # put the node as occupied in a list to prevent more than one connection with a lv-grid
                                #nodeIsOccupied.append(int(mvRow[0]))
                                nodeIsOccupied.append(id_mv)
                            else:
                                # node is free, so change node as occupied
                                nodeIsOccupied.append(id_mv)
            for lvRow in componentsDct['_'.join(['components_lv', str(iter2)])]:
                # wanted node should be a transformer
                if lvRow[1] == 'Transformer':
                    # search for the matching node
                    if int(lvRow[0]) == id_lv:
                        # delete slack and the associated connection in componentsDct['components_lv_'] and electricalgridsDct['elecgrid_lv']
                        for i in electricalgridsDct['_'.join(['elecgrid_lv', str(iter2)])]:
                            if int(i['id1']) == id_lv:
                                # id2 must be the slack node, delete it in componentsDct and then change id in electricalgridsDct
                                # search id2 in components_lv_
                                slack = int(i['id2'])
                                for j in componentsDct['_'.join(['components_lv', str(iter2)])]:
                                    if j[1] == 'Slack' and j[0] == slack:
                                        # delete slack node
                                        componentsDct['_'.join(['components_lv', str(iter2)])].remove(j)
                                # replace id2 (= slack) with the id of mv-node
                                i['id2'] = id_mv
                                newConnectionsDct['_'.join(['connection', str(iter2)])].append(i)
                                electricalgridsDct['_'.join(['elecgrid_lv', str(iter2)])].remove(i)
                            elif int(i['id2']) == id_lv:
                                # id1 must be the slack node, delete it in componentsDct and then change in electricalgridsDct
                                # search id1 in components_lv_
                                slack = int(i['id1'])
                                for j in componentsDct['_'.join(['components_lv', str(iter2)])]:
                                    if j[1] == 'Slack' and j[0] == slack:
                                        # delete slack node
                                        componentsDct['_'.join(['components_lv', str(iter2)])].remove(j)
                                # replace id1 (= slack) with the id of mv-node
                                i['id1'] = id_mv
                                newConnectionsDct['_'.join(['connection', str(iter2)])].append(i)
                                electricalgridsDct['_'.join(['elecgrid_lv', str(iter2)])].remove(i)
    # if there are still free nodes in mv then reconnect them with the deleted components
    # attention: consider the rules!
    # 0 = free; 1 = not free
    isFree = list()
    #isFree = None
    possibleConnection = list()
    for search in tempComponentsDct:
        componentsId = search[0]
        componentsType = search[1]
        for mvRow in componentsDct['_'.join(['components','mv'])]:
            if mvRow[1] == 'Node':
                # mvRow[0] is our possible connection -> 4
                if mvRow[0] not in nodeIsOccupied and mvRow[0] > 3:
                    for mvElecRow in electricalgridsDct['_'.join(['elecgrid', 'mv'])]:
                        # check all the connection at the possible connection node: mvRow[0]
                        #pdb.set_trace()
                        if mvElecRow['id1'] == mvRow[0]:
                            if mvElecRow['id2'] > 3:
                                # id2 = 5
                                id2 = int(mvElecRow['id2'])
                                for i in componentsDct['_'.join(['components', 'mv'])]:
                                    if i[0] == id2:
                                        if i[1] == 'Node':
                                            isFree.append(0)
                                        elif i[1] == 'PV':
                                            if componentsType != 'PV':
                                                isFree.append(0)
                                            else:
                                                isFree.append(1)
                                        elif i[1] == 'Biofuel':
                                            if componentsType != 'Biofuel':
                                                isFree.append(0)
                                            else:
                                                isFree.append(1)
                                        elif i[1] == 'CHP':
                                            if componentsType != 'CHP' or componentsType != 'HP':
                                                isFree.append(0)
                                            else:
                                                isFree.append(1)
                                        elif i[1] == 'Compensator':
                                            if componentsType != 'Compensator':
                                                isFree.append(0)
                                            else:
                                                isFree.append(1)
                                        elif i[1] == 'HP':
                                            if componentsType != 'CHP' or componentsType != 'HP':
                                                isFree.append(0)
                                            else:
                                                isFree.append(1)
                                        elif i[1] == 'Load':
                                            if componentsType != 'Load':
                                                isFree.append(0)
                                            else:
                                                isFree.append(1)
                                        elif i[1] == 'Wind':
                                            if componentsType != 'Wind':
                                                isFree.append(0)
                                            else:
                                                isFree.append(1)
                                        elif i[1] == 'Storage':
                                            if i[1] != 'Storage' and i[1] == 'PV' and i[1] == 'Wind' and i[1] == 'CHP':
                                                isFree.append(0)
                                            else:
                                                isFree.append(1)

                        elif mvElecRow['id2'] == mvRow[0]:
                            if mvElecRow['id1'] > 3:
                                id1 = int(mvElecRow['id1'])
                                for i in componentsDct['_'.join(['components', 'mv'])]:
                                    if i[0] == id1:
                                        if i[1] == 'Node':
                                            isFree.append(0)
                                        elif i[1] == 'PV':
                                            if componentsType != 'PV':
                                                isFree.append(0)
                                            else:
                                                isFree.append(1)
                                        elif i[1] == 'Biofuel':
                                            if componentsType != 'Biofuel':
                                                isFree.append(0)
                                            else:
                                                isFree.append(1)
                                        elif i[1] == 'CHP':
                                            if componentsType != 'CHP' or componentsType != 'HP':
                                                isFree.append(0)
                                            else:
                                                isFree.append(1)
                                        elif i[1] == 'Compensator':
                                            if componentsType != 'Compensator':
                                                isFree.append(0)
                                            else:
                                                isFree.append(1)
                                        elif i[1] == 'HP':
                                            if componentsType != 'CHP' or componentsType != 'HP':
                                                isFree.append(0)
                                            else:
                                                isFree.append(1)
                                        elif i[1] == 'Load':
                                            if componentsType != 'Load':
                                                isFree.append(0)
                                            else:
                                                isFree.append(1)
                                        elif i[1] == 'Wind':
                                            if componentsType != 'Wind':
                                                isFree.append(0)
                                            else:
                                                isFree.append(1)
                                        elif i[1] == 'Storage':
                                            if i[1] != 'Storage' and i[1] == 'PV' and i[1] == 'Wind' and i[1] == 'CHP':
                                                isFree.append(0)
                                            else:
                                                isFree.append(1)
                    # is there a 1 then node is not possible connection node!
                    if 1 not in isFree:
                        possibleConnection.append(mvRow[0])
                    else:
                        print(mvRow[0] + 'is not a possible connection node....')
        if len(possibleConnection) != 0:
            componentsDct['_'.join(['components', 'mv'])].append(search)
            newNode = random.choice(possibleConnection)
            for elecRow in tempElectricalgridsDct:
                if elecRow['id1'] == componentsId:
                    elecRow['id2'] = newNode
                    electricalgridsDct['elecgrid_mv'].append(elecRow)
                elif elecRow['id2'] == componentsId:
                    elecRow['id1'] = newNode
                    electricalgridsDct['elecgrid_mv'].append(elecRow)
    # now generate new scenario
    # first fill out newComponentsList and newElectricalgridsList with new ids
    # start with the ids by mv-grid
    # use newScenarioPath, newScenarioComponents, newScenarioElectricalGrid -> lists
    tempTransformerId = 0
    id = 1
    mvDct = {}
    lvDct = {}
    # iterate through componentsDct and electricalgridsDct
    # for every grid create a new hashMap
    #### mv-grid ####
    for mvRow in componentsDct['components_mv']:
        keyMv = str(mvRow[0])
        # value is the new id
        # structure: {'1':1, '2':2,....}
        mvDct[keyMv] = id
        # replace the new id
        mvRow[0] = id
        newScenarioComponents.append(mvRow)
        id = id + 1
        # create the corresponding electricalgrids/edges
        # check the ids over the mvDct
    for mvElecRow in electricalgridsDct['elecgrid_mv']:
        if mvDct.fromkeys(str(mvElecRow['id1'])):
            mvElecRow['id1'] = mvDct.get(str(mvElecRow['id1']))
            if mvDct.fromkeys(str(mvElecRow['id2'])):
                mvElecRow['id2'] = mvDct.get(str(mvElecRow['id2']))
                newScenarioElectricalGrid.append(mvElecRow)
        elif mvDct.fromkeys(str(mvElecRow['id2'])):
            mvElecRow['id2'] = mvDct.get(str(mvElecRow['id2']))
            if mvDct.fromkeys(str(mvElecRow['id1'])):
                mvElecRow['id1'] = mvDct.get(str(mvElecRow['id1']))
                newScenarioElectricalGrid.append(mvElecRow)

    #### lv-grids ####
    for iter1 in range(1, countGrids):
        #f = f + 1
        for lvRow in componentsDct['_'.join(['components_lv', str(iter1)])]:
            keyLv = str(lvRow[0])
            lvDct[keyLv] = id
            lvRow[0] = id
            if lvRow[1] == 'Transformer':
                tempTransformerId = lvRow[0]
            if lvRow[1] == 'Node':
                lvRow[2] = tempTransformerId
            newScenarioComponents.append(lvRow)
            id = id + 1
        for i in newConnectionsDct['_'.join(['connection', str(iter1)])]:
            # search id in mv to get the actual id
            if mvDct.fromkeys(str(i['id1'])):
                i['id1'] = mvDct.get(str(i['id1']))
                i['id2'] = lvDct.get(str(i['id2']))
                newScenarioElectricalGrid.append(i)
            elif mvDct.fromkeys(str(i['id2'])):
                i['id2'] = mvDct.get(str(i['id2']))
                i['id1'] = lvDct.get(str(i['id1']))
        for lvElecRow in electricalgridsDct['_'.join(['elecgrid_lv', str(iter1)])]:
            if lvDct.fromkeys(str(lvElecRow['id1'])):
                lvElecRow['id1'] = lvDct.get(str(lvElecRow['id1']))
                if lvDct.fromkeys(str(lvElecRow['id2'])):
                    lvElecRow['id2'] = lvDct.get(str(lvElecRow['id2']))
                    newScenarioElectricalGrid.append(lvElecRow)
            elif lvDct.fromkeys(str(lvElecRow['id2'])):
                lvElecRow['id2'] = lvDct.get(str(lvElecRow['id2']))
                if lvDct.fromkeys(str(lvElecRow['id1'])):
                    lvElecRow['id1'] = lvDct.get(str(lvElecRow['id1']))
                    newScenarioElectricalGrid.append(lvElecRow)
        lvDct.clear()

    # sort the ids respectively the given structure
    # new id
    newId = 1
    # first take slack, transformer and nodes
    for m in newScenarioComponents:
        if m[1] == 'Slack':
            newIdsDct[newId] = m[0]
            m[0] = newId
            newId += 1
        # transformer should always comes before a node
        if m[1] == 'Transformer':
            tempTransformerId = newId
            newIdsDct[newId] = m[0]
            m[0] = newId
            newId += 1
        if m[1] == 'Node':
            newIdsDct[newId] = m[0]
            m[0] = newId
            # third entry of node belongs the id of the transformer before
            m[2] = tempTransformerId
            newId += 1
    # second take all other components of the grid
    for m in newScenarioComponents:
        if m[1] != 'Slack' and m[1] != 'Transformer' and m[1] != 'Node':
            newIdsDct[newId] = m[0]
            m[0] = newId
            newId += 1

    # take newIdsDct to generate newScenarioElectricalgrid with new ids
    # get id1 and id2 to overwrite it with new ids
    # new ids are stored in newIdsDct and the keys are the new ids
    id1 = 0
    id2 = 0
    for m in newScenarioElectricalGrid:
        id1 = m['id1']
        id2 = m['id2']
        # overwrite the old ids
        m['id1'] = list(newIdsDct.keys())[list(newIdsDct.values()).index(id1)]
        m['id2'] = list(newIdsDct.keys())[list(newIdsDct.values()).index(id2)]

    print('generate new csv files.....')
    newScenarioComponents.sort(key=lambda k:k[0])
    csvReaderWriter.writeGrid(newScenarioPath, newScenarioComponents, newScenarioElectricalGrid)


# Function to attach the low voltage grid specified in 'lv_grids.csv' to the chosen medium voltage grid
def fullGridCreationRandom(newScenarioPath):
    print('Full grid creation started...\n')

    # Medium voltage grid is read and stored. The dcts for storing the corresponding components and cables are initialized
    pathMVGrid = '/'.join(('..', input('Medium voltage grid: ')))
    scenariosList.append(gridClass.Grid(path=pathMVGrid, pathComponents='/'.join((pathMVGrid, 'components.csv')),
                                        pathElectricalGrid='/'.join((pathMVGrid, 'el_grid.csv')), voltageLevel='mv',
                                        highestNodeId=0, highestComponentId=0))

    componentsDct['components_mv'] = []
    electricalgridsDct['elecgrid_mv'] = []

    # Read in mv grid, to be able to determine the number of free nodes
    csvReaderWriter.readGrid(scenariosList[0], componentsDct, electricalgridsDct)

    # Determine the possible connection nodes for low voltage grids in the created medium voltage grid.
    # startNode = 3, because Slack and Transformer can be ignored
    helperFunctions.determineConnectionNodes(scenariosList[0], 3, edgecountDct, connectionNodes, electricalgridsDct)

    # Inform user of the given number of 'free nodes'
    print('%i free nodes available...' % (len(connectionNodes)))

    # Get number of low voltage grids, the user wants to be generated
    numberOfLVGrids = int(input('How many LV grids should be created?'))

    # Resolve conflict, if user wishes to create more low voltage grids, than there are available nodes...
    multipleGridsToOneNode = 'n'
    if numberOfLVGrids > len(connectionNodes):
        multipleGridsToOneNode = input('Can multiple LV grids be connected to one node (y or n)?')
        if multipleGridsToOneNode == 'n':
            print('Wished number of LV grids is bigger than the number of free nodes!! Only %i low voltage grids will be attached!' % len(connectionNodes))
            numberOfLVGrids = len(connectionNodes)
        
    # Dict that lateron stores the amount of occurences for each individual lv grid
    quantityOfLVGridsDct = {}

    # List that holds all possible lv grids
    lvGridsList = list()
    # Read all possible low voltage grids from 'lv_grids.csv' file
    csvReaderWriter.readLVGrids(pathMVGrid, lvGridsList)
    # Process each possible lv grid...

    for grid in lvGridsList:
        # Low voltage grid is read and stored. The dcts for storing the corresponding components and cables are initialized.
        # Initialize number occurences with 0...
        quantityOfLVGridsDct[grid] = 0
        pathLVGrid = '/'.join(('..', grid))
        scenariosList.append(gridClass.Grid(path=pathLVGrid, pathComponents='/'.join((pathLVGrid, 'components.csv')),
                                            pathElectricalGrid='/'.join((pathLVGrid, 'el_grid.csv')), voltageLevel='lv',
                                            highestNodeId=0, highestComponentId=0, lvIndex=len(scenariosList)))

        componentsDct['_'.join(['components_lv', str(len(scenariosList) - 1)])] = []
        electricalgridsDct['_'.join(['elecgrid_lv', str(len(scenariosList) - 1)])] = []

    print('Grids created...')
    print('#####################\n')

    # Create Variables that store the amount of additional components, no matter if components are wished.
    # In this way, this name will be defined in any case (especially: no '-loads' given!)
    sumMVComponents = 0.0
    sumLVComponents = 0.0

    # Read in lv grids and store them in the provided dictionaries
    for scenario in scenariosList[1:]:
        # print(scenario)
        csvReaderWriter.readGrid(scenario, componentsDct, electricalgridsDct)

    # Search as many random nodes out of connection as lv grids should be generated --> remove them to prevent component creation
    for number in range(0, numberOfLVGrids):
        node = random.choice(connectionNodes)
        LVconnectionNodes.append(node)
        if multipleGridsToOneNode == 'n':
            connectionNodes.remove(node)
        # Search a random grid and increment its occurence
        quantityOfLVGridsDct[random.choice(lvGridsList)] += 1

    print(str(len(connectionNodes)) + ' connection nodes found')
    # Calculate offset for non-node components attached to the medium voltage grid
    # 'For every connection point that is connected with a low voltage grid, there will be one low voltage grid added
    componentOffsetMV = 0
    for counter, lvgrid in enumerate(lvGridsList):
        componentOffsetMV += quantityOfLVGridsDct[lvgrid] * (scenariosList[counter + 1].highestNodeId - 1)

    print('componentOffsetMV = %i' % (componentOffsetMV))

    # Create medium voltage grid
    for row in componentsDct['_'.join(['components', 'mv'])]:
        print(str(row[0]) + row[1])
        # If node found
        if row[1] == 'Node' or row[1] == 'Slack' or row[1] == 'Transformer':
            newScenarioComponents.append(row)
        # Component found
        else:
            # Any other component  
            row[0] = row[0] + componentOffsetMV
            newScenarioComponents.append(row)

    # Electrical grid creation...
    for row in electricalgridsDct['_'.join(['elecgrid', 'mv'])]:
        # Node to node connection ...
        if row['id2'] <= int(scenariosList[0].highestNodeId):
            print(str(row['id1']) + '->' + str(row['id2']))
            newScenarioElectricalGrid.append(row)
        # Node to component connection...
        else:
            row['id2'] = row['id2'] + componentOffsetMV
            newScenarioElectricalGrid.append(row)

    # Variabls needed for proper calculation of componentOffsets
    dictIndex = 0
    nodeOffsetLV = 0

    # Calculate the componentOffset for each low voltage grid
    componentOffsetLV = scenariosList[0].highestComponentId - scenariosList[0].highestNodeId

    for counter, grid in enumerate(lvGridsList):
        componentOffsetLV += quantityOfLVGridsDct[grid] * (scenariosList[counter + 1].highestNodeId - 1)

    topologyConfig = list()

    # Create low voltage grids for every connectionPoint
    for connectionNode in LVconnectionNodes:
        # Translate index into key by using the lvGridsList, which is an ordered object
        dictKey = lvGridsList[dictIndex]

        if quantityOfLVGridsDct[dictKey] == 0:
            dictIndex += 1
            dictKey = lvGridsList[dictIndex]
        # Translate index into key by using the lvGridsList, which is an ordered object

        print('Grid: %s' % (dictKey))

        quantityOfLVGridsDct[dictKey] -= 1
        currentLVGrid = '_'.join(['lv', str(dictIndex + 1)])

        topologyConfig.append((connectionNode, dictKey))

        # Component creation...
        for row in componentsDct['_'.join(['components', currentLVGrid])]:
            print(str(row[0]) + row[1])
            if row[1] == 'Transformer':
                # Create a copy to be able to edit its parameters without interfering with the read data
                tempRow = row.copy()
                # Calculate ID offset
                # 'Before this id, all nodes of the mv grid (sL[0].highestNodeId) plus for every already created lv grid (point), all nodes (sL[1].highestNodeID)
                # except for the Slack (-1). An additional -1 because, ??? im not sure anymore.. #TODO FIND OUT AGAIN!'
                tempRow[0] += scenariosList[0].highestNodeId + nodeOffsetLV - 1

                # Save new ID of transformer to pass as argument for following nodes...
                transformerId = tempRow[0]
                newScenarioComponents.append(tempRow)

            elif row[1] == 'Node':
                tempRow = row.copy()
                # Calculate ID offset
                tempRow[0] += scenariosList[0].highestNodeId + nodeOffsetLV - 1
                # Update corresponding transformer
                tempRow[2] = str(transformerId)
                newScenarioComponents.append(tempRow)

            elif row[1] != 'Slack':
                tempRow = row.copy()
                tempRow[0] += scenariosList[0].highestNodeId + (componentOffsetLV) - scenariosList[
                    dictIndex + 1].highestNodeId
                newScenarioComponents.append(tempRow)

        # Electrical grid creation...
        for row in electricalgridsDct['_'.join(['elecgrid', currentLVGrid])]:
            # Slack -> transformer edge has to be changed to 'connection point -> transformer'
            if row['id1'] == 1:
                tempRow = row.copy()
                tempRow['id1'] = connectionNode
                # Add id offset
                tempRow['id2'] += scenariosList[0].highestNodeId + nodeOffsetLV - 1
                newScenarioElectricalGrid.append(tempRow)

            elif row['id1'] <= scenariosList[dictIndex + 1].highestNodeId and row['id2'] <= scenariosList[
                dictIndex + 1].highestNodeId:
                # Node to Node edge found...
                tempRow = row.copy()
                tempRow['id1'] += scenariosList[0].highestNodeId + nodeOffsetLV - 1
                tempRow['id2'] += scenariosList[0].highestNodeId + nodeOffsetLV - 1
                newScenarioElectricalGrid.append(tempRow)

            else:
                tempRow = row.copy()
                tempRow['id1'] += scenariosList[0].highestNodeId + nodeOffsetLV - 1
                tempRow['id2'] += scenariosList[0].highestNodeId + (componentOffsetLV) - scenariosList[
                    dictIndex + 1].highestNodeId
                newScenarioElectricalGrid.append(tempRow)

        # Update componentoffset by adding the amount of generated nodes
        nodeOffsetLV += scenariosList[dictIndex + 1].highestNodeId - 1
        componentOffsetLV += scenariosList[dictIndex + 1].highestComponentId - scenariosList[
            dictIndex + 1].highestNodeId

    # Sort and write the 'topology_conf.csv' file
    topologyConfig.sort(key=lambda k: k[0])
    csvReaderWriter.topologyConfWrite(newScenarioPath, topologyConfig)
    # Sort components by their IDs before writing csv files!
    newScenarioComponents.sort(key=lambda k: k[0])

    # write new csv files
    csvReaderWriter.writeGrid(newScenarioPath, newScenarioComponents, newScenarioElectricalGrid)

# Recursive function to create a wished topology with given number of feeders and nodes per feeder on a given voltage level
def recTopologyCreation(feederType, feederDepth, voltageLevel, nextNodeId, feederFirstNodeId):
    # Read cable subtypes from csv-file and store them by name in dict
    cableSubTypesDct = {}
    csvReaderWriter.readCableSubTypes(cableSubTypesDct)

    transformerSubTypesDct = {}
    csvReaderWriter.readTransformerSubTypes(transformerSubTypesDct)

    numberOfFeeders = int(input('How many %ss should be created? ' % feederType))

    # Create Slack, Transformer and first busbar...
    if voltageLevel == 'lv':

        # Set voltagelevel dependent attribute(s)
        nodeAttribute = 400.0
        if feederType == 'feeder':
            # Set starting ID for first feeder manually
            feederFirstNodeId = 4
            # Determine transformer subtype
            while True:
                try:
                    transformer = transformerSubTypesDct[input('Type of transformer? ')]
                    # Inform user if transformer type does not match the voltage level
                    if ''.join(['_', voltageLevel]) in transformer[0].lower():
                        break
                    elif input('Transformer does not match voltage level... Continue? [y/n]') == 'y':
                        break
                    else:
                        pass

                except KeyError:
                    print('Unvalid type of transformer, try again...')

            # Create components
            newScenarioComponents.append((1, 'Slack', 20000))
            newScenarioComponents.append((2, 'Transformer', transformer))
            newScenarioComponents.append((3, 'Node', 2, 0, 0, nodeAttribute))

            # Create lossless cables
            cable = cableSubTypesDct['NO_LOSSES']
            newScenarioElectricalGrid.append({'id1': 1, 'id2': 2, 'cabletype': cable[0], 'arg1': cable[1],
                                              'arg2': cable[2], 'arg3': cable[3], 'arg4': cable[4], 'arg5': cable[5],
                                              'arg6': cable[6], 'arg7': cable[7]})
            newScenarioElectricalGrid.append({'id1': 2, 'id2': 3, 'cabletype': cable[0], 'arg1': cable[1],
                                              'arg2': cable[2], 'arg3': cable[3], 'arg4': cable[4], 'arg5': cable[5],
                                              'arg6': cable[6], 'arg7': cable[7]})


    elif voltageLevel == 'mv':

        # Set voltagelevel dependent attribute(s)
        nodeAttribute = 20000.0
        if feederType == 'feeder':
            # Set starting ID for first feeder manually
            feederFirstNodeId = 4

            # Determine transformer subtype
            while True:
                try:
                    transformer = transformerSubTypesDct[input('Type of transformer? ')]
                    # Inform user if transformer type does not match the voltage level
                    if ''.join(['_', voltageLevel]) in transformer[0].lower():
                        break
                    elif input('Transformer does not match voltage level... Continue? [y/n]') == 'y':
                        break
                    else:
                        pass

                except KeyError:
                    print('Unvalid type of transformer, try again...')

            # Create components
            newScenarioComponents.append((1, 'Slack', 110000))
            newScenarioComponents.append((2, 'Transformer', transformer))
            newScenarioComponents.append((3, 'Node', 2, 0, 0, nodeAttribute))

            # Create lossless cables
            cable = cableSubTypesDct['NO_LOSSES']
            newScenarioElectricalGrid.append({'id1': 1, 'id2': 2, 'cabletype': cable[0], 'arg1': cable[1],
                                              'arg2': cable[2], 'arg3': cable[3], 'arg4': cable[4], 'arg5': cable[5],
                                              'arg6': cable[6], 'arg7': cable[7]})
            newScenarioElectricalGrid.append({'id1': 2, 'id2': 3, 'cabletype': cable[0], 'arg1': cable[1],
                                              'arg2': cable[2], 'arg3': cable[3], 'arg4': cable[4], 'arg5': cable[5],
                                              'arg6': cable[6], 'arg7': cable[7]})

    else:
        # Couldn't resolve voltage level --> return...
        print('Error: %s is no valid voltage level...' % voltageLevel)
        return

    # For each wished feeder...
    for feeder in range(0, numberOfFeeders):
        # Read in number of nodes for current handled feeder
        if feederType == 'subfeeder':
            # Subfeeder -> display number of parent feeder as well as subfeeder index
            feederString = '.'.join([feederDepth, str(feeder + 1)])
        else:
            feederString = str(feeder + 1)

        numberOfNodes = int(input('Number of nodes %s %s: ' % (feederType, feederString)))

        # If feeder is not empty...
        if numberOfNodes > 0:
            if feederType == 'subfeeder':
                forkNodeId = int(input('Attach subfeeder at which node?'))
            elif feederType == 'feeder':
                forkNodeId = 3
            else:
                print('Error: %s is no valid feedertype...' % feederType)

            # Read in additional cable information...
            while True:
                try:
                    cable = cableSubTypesDct[input('Type of cable? ')]
                    break
                except KeyError:
                    print('Unvalid type of cable, try again...')

            cableLength = float(input('Length of cable in km? '))
            if input('Dynamic cable length? [y/n]... ') == 'y':
                cableLengthPerc = float(input('Wished variance in cable length in percentage [0.0-1.0]: '))
            else:
                # Default value, so all cables are the same length
                cableLengthPerc = 0

            # Create first node manually...
            actualCableLength = helperFunctions.dynCableLength(cableLength, cableLengthPerc)
            newScenarioComponents.append((feederFirstNodeId, 'Node', 2, 0, 0, nodeAttribute))
            newScenarioElectricalGrid.append(
                {'id1': forkNodeId, 'id2': feederFirstNodeId, 'cabletype': cable[0], 'arg1': actualCableLength,
                 'arg2': cable[2], 'arg3': cable[3], 'arg4': cable[4], 'arg5': cable[5], 'arg6': cable[6],
                 'arg7': cable[7]})

            nextNodeId = feederFirstNodeId + 1

            # Create all following nodes...
            for node in range(nextNodeId, feederFirstNodeId + numberOfNodes):
                actualCableLength = helperFunctions.dynCableLength(cableLength, cableLengthPerc)
                newScenarioComponents.append((node, 'Node', 2, 0, 0, nodeAttribute))
                newScenarioElectricalGrid.append(
                    {'id1': node - 1, 'id2': node, 'cabletype': cable[0], 'arg1': actualCableLength,
                     'arg2': cable[2], 'arg3': cable[3], 'arg4': cable[4], 'arg5': cable[5], 'arg6': cable[6],
                     'arg7': cable[7]})

            # Update ID of next node and the new start node for following feeders
            nextNodeId = feederFirstNodeId + numberOfNodes
            feederFirstNodeId = nextNodeId

            if input('Create subfeeders in feeder %s? ' % feederString) == 'y':
                returnTuple = recTopologyCreation('subfeeder', feederString, voltageLevel, nextNodeId,
                                                  feederFirstNodeId)
                nextNodeId = returnTuple[0]
                feederFirstNodeId = returnTuple[1]
    return (nextNodeId, feederFirstNodeId)


# Recursive function to create a wished topology with given number of feeders and nodes per feeder on a given voltage level
def autoTopologyCreation(voltageLevel, numberOfFeeders, numberOfNodes, transformer, cableType, cableLength):
    nextNodeId = 1
    feederFirstNodeId = 1
    # Read cable subtypes from csv-file and store them by name in dict
    cableSubTypesDct = {}
    csvReaderWriter.readCableSubTypes(cableSubTypesDct)

    transformerSubTypesDct = {}
    csvReaderWriter.readTransformerSubTypes(transformerSubTypesDct)

    transformer = transformerSubTypesDct[transformer]

    # Create Slack, Transformer and first busbar...
    if voltageLevel == 'lv':

        # Set voltagelevel dependent attribute(s)
        nodeAttribute = 400.0
        # Set starting ID for first feeder manually
        feederFirstNodeId = 4

        # Create components
        newScenarioComponents.append((1, 'Slack', 20000))
        newScenarioComponents.append((2, 'Transformer', transformer))
        newScenarioComponents.append((3, 'Node', 2, 0, 0, nodeAttribute))

        # Create lossless cables
        cable = cableSubTypesDct['NO_LOSSES']
        newScenarioElectricalGrid.append({'id1': 1, 'id2': 2, 'cabletype': cable[0], 'arg1': cable[1],
                                          'arg2': cable[2], 'arg3': cable[3], 'arg4': cable[4], 'arg5': cable[5],
                                          'arg6': cable[6], 'arg7': cable[7]})
        newScenarioElectricalGrid.append({'id1': 2, 'id2': 3, 'cabletype': cable[0], 'arg1': cable[1],
                                          'arg2': cable[2], 'arg3': cable[3], 'arg4': cable[4], 'arg5': cable[5],
                                          'arg6': cable[6], 'arg7': cable[7]})


    elif voltageLevel == 'mv':

        # Set voltagelevel dependent attribute(s)
        nodeAttribute = 20000.0
        # Set starting ID for first feeder manually
        feederFirstNodeId = 4

        # Create components
        newScenarioComponents.append((1, 'Slack', 110000))
        newScenarioComponents.append((2, 'Transformer', transformer))
        newScenarioComponents.append((3, 'Node', 2, 0, 0, nodeAttribute))

        # Create lossless cables
        cable = cableSubTypesDct['NO_LOSSES']
        newScenarioElectricalGrid.append({'id1': 1, 'id2': 2, 'cabletype': cable[0], 'arg1': cable[1],
                                          'arg2': cable[2], 'arg3': cable[3], 'arg4': cable[4], 'arg5': cable[5],
                                          'arg6': cable[6], 'arg7': cable[7]})
        newScenarioElectricalGrid.append({'id1': 2, 'id2': 3, 'cabletype': cable[0], 'arg1': cable[1],
                                          'arg2': cable[2], 'arg3': cable[3], 'arg4': cable[4], 'arg5': cable[5],
                                          'arg6': cable[6], 'arg7': cable[7]})


    else:
        # Couldn't resolve voltage level --> return...
        print('Error: %s is no valid voltage level...' % voltageLevel)
        return

    actualCableLength = cableLength

    # If only one feeder should be created, decrement numberOfNodes to compensate manually created busbar...
    if numberOfFeeders == 1:
        numberOfNodes -= 1

    # For each wished feeder...
    for feeder in range(0, numberOfFeeders):

        cable = cableSubTypesDct[cableType]

        # If feeder is not empty...
        if numberOfNodes > 0:

            # TODO CHANGE IF WISHED!
            # actualCableLength = helperFunctions.dynCableLength(cableLength, cableLengthPerc)

            newScenarioComponents.append((feederFirstNodeId, 'Node', 2, 0, 0, nodeAttribute))
            newScenarioElectricalGrid.append(
                {'id1': 3, 'id2': feederFirstNodeId, 'cabletype': cable[0], 'arg1': actualCableLength,
                 'arg2': cable[2], 'arg3': cable[3], 'arg4': cable[4], 'arg5': cable[5], 'arg6': cable[6],
                 'arg7': cable[7]})

            nextNodeId = feederFirstNodeId + 1

            # Create all following nodes...
            for node in range(nextNodeId, feederFirstNodeId + numberOfNodes):
                # TODO CHANGE IF WISHED!
                # actualCableLength = helperFunctions.dynCableLength(cableLength, cableLengthPerc)
                newScenarioComponents.append((node, 'Node', 2, 0, 0, nodeAttribute))
                newScenarioElectricalGrid.append(
                    {'id1': node - 1, 'id2': node, 'cabletype': cable[0], 'arg1': actualCableLength,
                     'arg2': cable[2], 'arg3': cable[3], 'arg4': cable[4], 'arg5': cable[5], 'arg6': cable[6],
                     'arg7': cable[7]})

            # Update ID of next node and the new start node for following feeders
            nextNodeId = feederFirstNodeId + numberOfNodes
            feederFirstNodeId = nextNodeId

    return (nextNodeId, feederFirstNodeId)

# Function to merge two scenarios. new scenario has the same topology as the first input grid and contains components from both grids
def mergeScenarios(newScenarioPath):
    print('Scenario merge started...\n')

    # first grid is read and stored. The dcts for storing the corresponding components and cables are initialized
    pathGrid0 = '/'.join(('..', input('First grid: ')))
    scenariosList.append(gridClass.Grid(path=pathGrid0, pathComponents='/'.join((pathGrid0, 'components.csv')),
                                        pathElectricalGrid='/'.join((pathGrid0, 'el_grid.csv')), voltageLevel='0',
                                        highestNodeId=0, highestComponentId=0))
    componentsDct['components_0'] = []
    electricalgridsDct['elecgrid_0'] = []
    # Read in the first grid
    csvReaderWriter.readGrid(scenariosList[0], componentsDct, electricalgridsDct)

    # second grid is read and stored. The dcts for storing the corresponding components and cables are initialized
    pathGrid1 = '/'.join(('..', input('Second grid: ')))
    scenariosList.append(gridClass.Grid(path=pathGrid1, pathComponents='/'.join((pathGrid1, 'components.csv')),
                                        pathElectricalGrid='/'.join((pathGrid1, 'el_grid.csv')), voltageLevel='1',
                                        highestNodeId=0, highestComponentId=0))
    componentsDct['components_1'] = []
    electricalgridsDct['elecgrid_1'] = []
    # Read in the first grid
    csvReaderWriter.readGrid(scenariosList[1], componentsDct, electricalgridsDct)

    if scenariosList[0].highestNodeId != scenariosList[1].highestNodeId:
        print('Warning: The two scenarios have a different number of nodes')

    # Copy first grid to result grid
    for row in componentsDct['_'.join(['components', '0'])]:
        newScenarioComponents.append(row)
    for row in electricalgridsDct['_'.join(['elecgrid', '0'])]:
        newScenarioElectricalGrid.append(row)

    # highest component id in first grid
    newComponentID = scenariosList[0].highestComponentId

    # loop through connections in second grid
    for con in electricalgridsDct['_'.join(['elecgrid', '1'])]:
        # check if conection is between node and component
        if con['id1'] <= scenariosList[1].highestNodeId and con['id2'] <= scenariosList[1].highestNodeId:
            continue
        nodeID = con['id1']
        compID = con['id2']
        if nodeID > scenariosList[1].highestNodeId:
            nodeID = con['id2']
            compID = con['id1']
        # check if node is present in first grid
        if nodeID > scenariosList[0].highestNodeId:
            continue
        
        # make new component with new id
        component = componentsDct['_'.join(['components', '1'])][compID-1]
        newComponentID += 1
        component[0] = newComponentID
        newScenarioComponents.append(component)

        # make connection to new component
        newCon = con
        if nodeID == newCon['id1']:
            newCon['id2'] = newComponentID
        else:
            newCon['id1'] = newComponentID
        newScenarioElectricalGrid.append(newCon)