###############################################################################
# Helper function module
#
# @author Sonja Happ <sonja.happ@eonerc.rwth-aachen.de>, Felix Wege
# @copyright 2019, Institute for Automation of Complex Power Systems, EONERC
# @license GNU General Public License (version 3)
#
# DistAIX Scenario Generator
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#############################################################################

# This module is used to provide helper functions for the scenarioGenerator

import random
import copy

from collections import Counter

# function checks if the connected lv-node is a transformer
def checkLvConnection(connectionsDct, componentsDct, countGrids):
    transformerIsGiven = None
    nodes = list()
    # search for transformer
    for i in range(1,countGrids):
        for search in componentsDct['_'.join(['components_lv', str(i)])]:
            if search[1] == 'Transformer':
                nodes.append(search[0])
        for keys in connectionsDct['_'.join(['wishedConnections_mv_lv', str(i)])]:
            tempNode = int(keys[1])
            while not transformerIsGiven:
                if tempNode in nodes:
                    transformerIsGiven = True
                else:
                    transformerIsGiven = False
                    while tempNode not in nodes:
                        print('ATTENTION: lv node is not existing!')
                        print(keys[1])
                        print('Please change node in lv-grid with the following options: ')
                        print(nodes)
                        keys[1] = str(input('please give a new connection node in lv: '))
                        tempNode = int(keys[1])
                        transformerIsGiven = True

# function checks if there are more then one connection with a mv node
def checkUniqueness(grid,connectionsDct, componentsDct):
    # list should give an overview for the user - which node can be used
    nodes = list()
    for search in componentsDct['_'.join(['components', grid.voltageLevel])]:
        if search[1] == 'Node':
            nodes.append(search[0])
    tempList = list()
    tempList = copy.deepcopy(nodes)
    counter = 0
    for key in connectionsDct.values():
        for iterKeys in connectionsDct.values():
            if key[0][0] == iterKeys[0][0]:
                counter += 1
                if counter > 1:
                    tempList.remove(key[0][0])
                    print('There is more than one connection.')
                    print(key[0])
                    print('Please change node in mv-grid with the following options: ')
                    print(tempList)
                    key[0][0] = int(input('input: '))
        counter = 0
        
# function checks existence of connection nodes on the mv side
# it has to be a 'Node' not a slack or transformer or component
def checkMvConnection(grid,connectionsDct,componentsDct):
    nodesAreGiven = None
    # just get ids if id is a type of 'Node'
    nodes = list()
    for i in componentsDct['_'.join(['components', grid.voltageLevel])]:
        if i[1] == 'Node':
            nodes.append(i[0])
    for keys in connectionsDct.values():
        tempNode = keys[0][0]
        while not nodesAreGiven:
            if tempNode in nodes:
                nodesAreGiven = True
            else:
                nodesAreGiven = False
                while tempNode not in nodes:
                    print('ATTENTION: mv node is not existing!')
                    print(keys[0][0])
                    print('Please change node in mv-grid with the following options: ')
                    print(nodes)
                    keys[0][0] = int(input('please give a new connection node in mv: '))
                    tempNode = keys[0][0]
                nodesAreGiven = True

# function to make node as a free node
def getFreeNode(grid, freeNode, tempComponentsDct, tempElectricalgridsDct, electricalgridsDct, componentsDct):
    for row in electricalgridsDct['_'.join(['elecgrid', grid.voltageLevel])]:
        if int(row['id1']) ==  freeNode:
            if row['id2'] > grid.highestNodeId:
                # save edge in tempElectricalGridsDct for later use
                tempElectricalgridsDct.append(row)
                # delete the entry in electricalgridsDct['elecgrid_mv']
                electricalgridsDct['_'.join(['elecgrid', grid.voltageLevel])].remove(row)
                for comRow in componentsDct['_'.join(['components', grid.voltageLevel])]:
                    # just check the id in componentsDct
                    if int(row['id2']) == comRow[0]:
                        tempComponentsDct.append(comRow)                       
                        componentsDct['_'.join(['components', grid.voltageLevel])].remove(comRow)
        elif int(row['id2']) == freeNode:
            if row['id1'] > grid.highestNodeId:
                tempElectricalgridsDct.append(row)
                electricalgridsDct['_'.join(['elecgrid', grid.voltageLevel])].remove(row)

                for comRow in componentsDct['_'.join(['components', grid.voltageLevel])]:
                    # just check the id in componentsDct
                    if int(row['id1']) == comRow[0]:
                        tempComponentsDct.append(comRow)
                        componentsDct['_'.join(['components', grid.voltageLevel])].remove(comRow)

# modify 'def countOutgoingEdges':count the number of outgoing edges in coloum 1 (id1) and coloum 2 (id2)
def modifyCountOutgoingEdges(elGrid, grid, electricalgridsDct, edgecountDct1, edgecountDct2):

    for edge in electricalgridsDct[elGrid]:
        # id1 is connect to an component
        if edge['id1'] <= grid.highestNodeId and edge['id2'] >= grid.highestNodeId:
            edgecountDct1[edge['id1']] += 1
    for edge in electricalgridsDct[elGrid]:
        if edge['id2'] <= grid.highestNodeId and edge['id1'] >= grid.highestNodeId:
            edgecountDct2[edge['id2']] += 1

# check if a given node is free
def determineFreeNode(grid, startNode, searchNode, edgecountDct1, edgecountDct2, isConnectionNode, electricalgridsDct):
    # Clear storing dicts
    edgecountDct1.clear()
    edgecountDct2.clear()


    # and initialize edgecountDct with zeroes with the number of nodes
    for x in range(1, grid.highestNodeId + 1):
        edgecountDct1[x] = 0
    for y in range(1, grid.highestNodeId + 1):
        edgecountDct2[y] = 0
    #pdb.set_trace()
    modifyCountOutgoingEdges('_'.join(('elecgrid', grid.voltageLevel)),grid, electricalgridsDct, edgecountDct1, edgecountDct2)

    if searchNode > startNode:
        if searchNode in edgecountDct1:
            if edgecountDct1[searchNode] < 1:
                if searchNode in edgecountDct2:
                    edgecountDct2[searchNode] < 1
                    # if condition is true then node is free
                    isConnectionNode.append(searchNode)


# Function that determines the number of outgoing edges of a grid based on its elec_grid.csv file
def countOutgoingEdges(elGrid, electricalgridsDct, edgecountDct):
    # Using a counter object to count how often an edge exists with every id as first parameter
    edges = Counter(k['id1'] for k in electricalgridsDct[elGrid] if k.get('id1'))
    for edge, count in edges.most_common():
        edgecountDct[edge] = count
        #print(edgecountDct)

# Function that determines all possible connection points. This is done by counting outgoing edges.
# If a node has less than 2 outgoing edges, it is a possible candidate and therefore stored in a list connectionNodes.
# After determining all candidates, all of them which are already occupied by a non-node component are removed from the list.
# The additional argument startNode is used to determine a lower bound. All nodes with lower IDs are not considered.
def determineConnectionNodes(grid,startNode, edgecountDct, connectionNodes, electricalgridsDct):
    # Clear storing dicts and initialize edgecountDct with zeroes
    edgecountDct.clear()
    connectionNodes.clear()
    for x in range(1, grid.highestNodeId + 1):
        edgecountDct[x] = 0
    
    # Compute outgoing edges
    countOutgoingEdges('_'.join(('elecgrid',grid.voltageLevel)),electricalgridsDct,edgecountDct)
    # Add each id that is higher than startNode and has less than two outgoing edges to connectionNodes
    for key in edgecountDct:
        if int(key) > startNode and int(edgecountDct[key]) < 2:
            connectionNodes.append(int(key))

    # Iterate over all edges and remove nodes that are already connected with components from connectionNodes
    for row in electricalgridsDct['_'.join(['elecgrid', grid.voltageLevel])]:
        if row['id2'] > grid.highestNodeId:
            if row['id1'] in connectionNodes:
                connectionNodes.remove(row['id1'])

    #print(connectionNodes)


# Function to determine variable cable lenghts when creating net topologies
def dynCableLength(cableLength, cableLengthPerc):
    
    random.seed()

    perc = cableLength * cableLengthPerc
    randLength = round(random.uniform((cableLength - perc), (cableLength + perc)), 6)
    
    return randLength

# Function to add an aditional column for nodes indicating if the node is allowed to participate in communication
def addCommunicationPerc(grid, componentsDct):

    random.seed()

    percentage = float(input('What percentage of nodes should participate in communication? [0.0-100.0]: '))
    print(componentsDct)
    keys = list(componentsDct.keys())
    for node in componentsDct[keys[0]]:
        print('Node:')
        print(node)
        if 3 <= node[0] <= grid.highestNodeId:
            if random.uniform(0.0,100.0) <= percentage:
                node.insert(5,'1')
            else:
                node.insert(5,'0')

# Function to add an aditional column for nodes indicating if the node is allowed to participate in communication
def addCommunicationPercNew(grid, componentsDct, edgecountDct):

    keys = list(componentsDct.keys())
    for node in componentsDct[keys[0]]:

        # Transformer flags initialized with 1 by default
        if node[0] == 2:
            node.insert(5,'1')

        elif 3 <= node[0] <= grid.highestNodeId:
            # Nodes which are no busbars are initialized with 0
            if edgecountDct[node[0]] < 2:
                node.insert(5,'0')
            # Busbars initialized with 1
            else:
                node.insert(5, '1')