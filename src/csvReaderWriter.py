###############################################################################
# csv Reader and Writer Module
#
# @author Sonja Happ <sonja.happ@eonerc.rwth-aachen.de>, Felix Wege
# @copyright 2019, Institute for Automation of Complex Power Systems, EONERC
# @license GNU General Public License (version 3)
#
# DistAIX Scenario Generator
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#############################################################################

# This module is used to read in and write csv files containing the grids.

import csv
import os
from pathlib import Path
###############################################
# Read functions...

# extension of def readGrid()
def readGridExtension(scenario, componentsDct, electricalgridsDct, connectionsDct,m):
    componentsCSVRead(scenario, componentsDct)
    electricalGridCSVRead(scenario, electricalgridsDct)
    readWishedConnectionFromCSV(scenario, connectionsDct,m)

# Reading the wished Connection between primary and secondary site
# with header: id_mv | id_lv
def readWishedConnectionFromCSV(scenario, wishedConnectionDct,m):
    print('Reading wished connection from csv file...\n')
    with open (scenario.pathWishedConnections, 'r') as csvfile:
        # save the file row wise in a save vector
        saveVec = csv.reader(csvfile, delimiter=',')

        for row in saveVec:
            # key: id_mv, value: id_lv
            row[0] = int(row[0])
            wishedConnectionDct['_'.join(['wishedConnections_mv_lv',str(m)])].append(row)


# Reading the component file
def componentsCSVRead(scenario, componentsDct):

    print('Reading component files...\n')
    print('Opening ' + scenario.pathComponents + '...\n')

    if scenario.lvIndex is not None:
        voltageLevel = '_'.join([scenario.voltageLevel,str(scenario.lvIndex)])
    else:
        voltageLevel = scenario.voltageLevel

    # Variables for highestId determinations
    highestNodeId = 0
    highestComponentId = 0
    # Open file at path...
    with open(scenario.pathComponents, 'r') as csvfile:
        # Save the file row wise in a save vector
        saveVec = csv.reader(csvfile, delimiter=',')
        for row in saveVec:
            # cast all ids to integer representation
            row[0] = int(row[0])
            # Add component to corresponding component dictionary entry
            componentsDct['_'.join(['components', voltageLevel])].append(row)

            # Determine highest node and component ids
            if row[1] == 'Slack' or row[1] == 'Transformer' or row[1] == 'Node':
                if row[0] > highestNodeId:
                    highestNodeId = row[0]
            else:
                if row[0] > highestComponentId:
                    highestComponentId = row[0]

        if highestComponentId < highestNodeId:
            highestComponentId = highestNodeId
            
        scenario.highestComponentId = highestComponentId
        scenario.highestNodeId = highestNodeId

        print('highestNodeId_%s ' % voltageLevel + ' = ' + str(scenario.highestNodeId))

# Reading electrical grids...
def electricalGridCSVRead(scenario,electricalgridsDct):

    if scenario.lvIndex is not None:
        voltageLevel = '_'.join([scenario.voltageLevel,str(scenario.lvIndex)])
    else:
        voltageLevel = scenario.voltageLevel

    print('Reading electrical grid files...\n')
    print('Opening ' + scenario.pathElectricalGrid + '...\n')
    # Open file at path...
    with open(scenario.pathElectricalGrid, 'r') as csvfile:
        # Save the file row wise in a save vector
        saveVec = csv.DictReader(csvfile, fieldnames = ('id1', 'id2', 'cabletype', 'arg1', 'arg2', 'arg3', 'arg4', 'arg5', 'arg6', 'arg7'), delimiter=',')
        for row in saveVec:
            # Cast all parameters to integer representation
            row['id1'] = int(row['id1'])
            row['id2'] = int(row['id2'])
            row['cabletype'] = row['cabletype']
            # Add edge to the corresponding dictionary
            electricalgridsDct['_'.join(['elecgrid', voltageLevel])].append(row)

# Joined fucntion for reading the whole grid...
def readGrid(scenario, componentsDct, electricalgridsDct):

    componentsCSVRead(scenario,componentsDct)
    electricalGridCSVRead(scenario, electricalgridsDct)

# Function that reads and stores all component subtypes...
def readComponentSubTypes(subTypesDct, auto):

    print('Reading subtype files...\n')
    subTypes = ['load', 'chp', 'compensator', 'ev', 'pv', 'storage', 'wind', 'hp', 'biofuel']
    for subType in subTypes:
        for voltageLevel in ['lv', 'mv']:
            subTypesDct['_'.join([subType,voltageLevel])] = []
            if auto:
                # Only loads currently supported for automated component creation
                if subType == 'load':
                    with open('/'.join(['../subtypes', '.'.join(['_'.join([voltageLevel,'auto']),'csv'])]), 'r') as csvfile:
                        saveVec = csv.reader(csvfile, delimiter=',')
                        # Skip Header
                        next(saveVec)
                        for row in saveVec:
                            subTypesDct['_'.join([subType,voltageLevel])].append(row)
            else:
                with open('/'.join(['../subtypes', '.'.join(['_'.join([subType,voltageLevel]),'csv'])]), 'r') as csvfile:
                    saveVec = csv.reader(csvfile, delimiter=',')
                    # Skip Header
                    next(saveVec)
                    for row in saveVec:
                        subTypesDct['_'.join([subType,voltageLevel])].append(row)

# Function that reads and stores all cable subtypes...
def readCableSubTypes(cableSubTypesDct):

    print('Reading cable subtypes... \n')
    with open('../subtypes/cable.csv', 'r') as csvfile:
        saveVec = csv.reader(csvfile, delimiter=',')
        # Skip Header
        next(saveVec)
        for row in saveVec:
            # Store each subtype with its 'name' as key
            cableSubTypesDct[row[0]] = row

# Function that reads and stores transformer subtypes...
def readTransformerSubTypes(transformerSubTypesDct):
    print('Reading transformer subtypes... \n')
    with open('../subtypes/transformer.csv', 'r') as csvfile:
        saveVec = csv.reader(csvfile, delimiter=',')
        # Skip Header
        next(saveVec)
        for row in saveVec:
            #Store each subtype with its 'name' as key
            transformerSubTypesDct[row[0]] = row

# Function that reads and stores the percentage of occurrence for all subtypes...
def readSubtTypesPercentages(scenario, subTypesPercDct):

    print('Reading subtype percentages... \n')
    with open('/'.join([scenario.path,'subtypes_perc.csv'])) as csvfile:
        saveVec = csv.reader(csvfile,delimiter=',')
        for row in saveVec:
            # Store all values as floats
            subTypesPercDct[row[0]] = [float(i) for i in row[1:]]


# Function that reads and stores the wished nominal powers for all component types...
def readNominalPowerFromCSV(voltageLevel, wishedKWperComponentDct, pathGrid):

    print('Reading wished nominal powers from csv file... \n')
    with open('/'.join([pathGrid, 'wished_nominal_power.csv'])) as csvfile:
        saveVec = csv.reader(csvfile, delimiter=',')
        for row in saveVec:
            wishedKWperComponentDct['_'.join([row[0],voltageLevel])] = float(row[1])


# Function that reads in all possible low voltage grids from 'lv_grids.csv'
def readLVGrids(pathMVGrid,lvGridsList):

    print('Reading possible low voltage grids...')
    with open('/'.join([pathMVGrid,'lv_grids.csv']), 'r') as csvfile:
        saveVec = csv.reader(csvfile,delimiter=',')
        # Remove additional/unnecessary symbols from file...
        for row in saveVec:
            row = str(row)
            row = row.replace('[','')
            row = row.replace(']','')
            row = row.replace('\'', '')
            lvGridsList.append(row)
            
# Function that reads in wished grids for automated topology creation from given csv file
def readAutoGenerationGrids(autoGenerationGridsDct, filePath):

    print('Reading grids for auto topology generation...')
    with open(filePath) as csvfile:
        saveVec = csv.reader(csvfile,delimiter=',')
        # Skip Header
        next(saveVec)
        for row in saveVec:
            if '#' not in row[0]:
                autoGenerationGridsDct[row[0]] = row[1:]
##############################################################
# Write functions...
# Write component file
def componentCSVWrite(newScenarioPath, newScenarioComponents):
    # Open a new file at path...
    with open('/'.join((newScenarioPath,'componentsTEMP.csv')), 'w') as csvfile:
        # Init writer object
        writer = csv.writer(csvfile, delimiter=',', lineterminator = '\n')
        # Write all rows to new file
        for component in newScenarioComponents:
            writer.writerow(component)

# Write electrical grid
def electricalGridCSVWrite(newScenarioPath,newScenarioElectricalGrid):
    # Open a new file at path
    with open('/'.join((newScenarioPath,'el_grid.csv')), 'w') as csvfile:
        # Init writer object
        writer = csv.DictWriter(csvfile, fieldnames = ('id1', 'id2', 'cabletype', 'arg1', 'arg2', 'arg3', 'arg4', 'arg5', 'arg6', 'arg7'), delimiter=',', lineterminator = '\n')
        # Wrote all rows to new file
        for edge in newScenarioElectricalGrid:
            writer.writerow({'id1': edge['id1'], 'id2': edge['id2'], 'cabletype': edge['cabletype'], 
                                'arg1':edge['arg1'], 'arg2':edge['arg2'], 'arg3':edge['arg3'],
                                'arg4':edge['arg4'],'arg5':edge['arg5'],'arg6':edge['arg6'],'arg7':edge['arg7']})

# Write topology config file
def topologyConfWrite(newScenarioPath,topologyConfig):

    if not Path(newScenarioPath).is_dir():
        os.mkdir(newScenarioPath)

    with open('/'.join((newScenarioPath,'topology_conf.csv')), 'w') as csvfile:
        # Init writer object
        writer = csv.DictWriter(csvfile,fieldnames = ('node', 'grid'), delimiter = ',', lineterminator = '\n')
        # Write all rows of topologyConf to new file
        for row in topologyConfig:
            writer.writerow({'node': row[0], 'grid': row[1]})

# Write nominal power file
def writeNominalPower(newScenarioPath, actualKWperComponentDct):
    if not Path(newScenarioPath).is_dir():
        os.mkdir(newScenarioPath)
    with open('/'.join((newScenarioPath, 'nominal_power.csv')), 'w') as csvfile:
        # Init writer object
        writer = csv.writer(csvfile, delimiter=',', lineterminator = '\n')
        # Write all rows of actualKWperComponentDct to new file
        for key, value in actualKWperComponentDct.items():
            writer.writerow([key,value])
            
# Joined function to write the whole grid
def writeGrid(newScenarioPath, newScenarioComponents, newScenarioElectricalGrid):
    if not Path(newScenarioPath).is_dir():
        os.mkdir(newScenarioPath)
    componentCSVWrite(newScenarioPath, newScenarioComponents)
    electricalGridCSVWrite(newScenarioPath, newScenarioElectricalGrid)
    
    # Read file again and remove unnecessary symbols resulting from subtypes with variable length
    with open('/'.join((newScenarioPath,'componentsTEMP.csv')), 'r') as infile:
         data = infile.read()
         data = data.replace('"','')
         data = data.replace('[','')
         data = data.replace(']','')
         data = data.replace('\'', '')
         data = data.replace(' ','')
    with open('/'.join((newScenarioPath,'components.csv')), 'w') as outfile:
         outfile.write(data)

    os.remove('/'.join((newScenarioPath,'componentsTEMP.csv')))
    
