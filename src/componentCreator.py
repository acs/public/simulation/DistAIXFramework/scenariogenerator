###############################################################################
# Component creator module
#
# @author Sonja Happ <sonja.happ@eonerc.rwth-aachen.de>, Felix Wege
# @copyright 2019, Institute for Automation of Complex Power Systems, EONERC
# @license GNU General Public License (version 3)
#
# DistAIX Scenario Generator
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
###############################################################################


# This module is used to create all components for the corresponding grids.
# This is done by searching a random node out of the set of non-occupied nodes.
# For each of these nodes, the wished component is created and attached to it.
# The create() function is called for EVERY component type in the main script.
# There are several rules, which have to be followed:
# 1) HP and CHP not allowed to be attached to the same node
# 2) Windturbines and Biofuel plants only in medium voltage grid
# 3) Electrical vehicles only in low voltage grid
# 4) Storages only at nodes, where also a producer component (PV, Wind, CHP) is attached, except user explicitly wants it!
# 5) At medium voltage nodes, where a low voltage subgrid is attached, no components should be created
# 6) Only one component of the same type per node


import random

def create(voltage, onlyLV, actualKWperComponentDct, numberOfComponents, componentType, connectionNodes, hpChp,
           producerComponents, componentOffsetMV, scenariosList, componentsDct,
           electricalgridsDct, subTypesListDct, newScenarioComponents, newScenarioElectricalGrid, communicationPercDct):
 
    # Seed random function
    random.seed()
    # List to store already handled nodes for each component type (Rule 6!)
    handledNodes = list()

    # Load differentiation and setting of the right subtype...
    if componentType == 'storage_mv' or componentType == 'storage_lv':
        # Allow user to chose creation of more Storages than available nodes with producers attached
        additionalStorages = False
        StorageCounter = 0
        # If there are more Storages than producer nodes
        if numberOfComponents > len(producerComponents):
                # If yes, set additionalStorages flag
                if input('More Storages than producers given, generate additional Storages nonetheless?[y/n]') == 'y':
                    additionalStorages = True
                # Else just create as many Storages as nodes with producer components attachend
                else:
                    numberOfComponents = len(producerComponents)

    # For the number of wished components
    if numberOfComponents > len(connectionNodes):
            numberOfComponents = len(connectionNodes)

    for component in range(0,numberOfComponents):
        
        # Search for a random node, which follows all given rules
        node = random.choice(connectionNodes)
        # Rule 1!
        if componentType == 'chp_mv' or componentType == 'chp_lv':
            
            # Take random subtype            
            componentTypeShort = 'CHP'

            if voltage == 'mv':
                subtype = subTypesListDct['chp_mv'].pop()
            elif voltage == 'lv':
                subtype = subTypesListDct['chp_lv'].pop()

            while node in handledNodes or node in hpChp:
                node = random.choice(connectionNodes)
            handledNodes.append(node)
            hpChp.append(node)
            # CHP also needs to be added to the list of producer Components (Rule 4!)
            if node not in producerComponents:
                producerComponents.append(node)
        # Rule 1!
        elif componentType == 'hp_mv' or componentType == 'hp_lv':

            # If all free nodes are already occupied by chp or hp, stop creation of further hp components
            if len(hpChp) >= len(connectionNodes):
                if component == 0:
                    print('WARNING! No HP components created. All free nodes already occupied by CHP components...')
                break

            componentTypeShort = 'HP'

            # Take subtype
            if voltage == 'mv':
                subtype = subTypesListDct['hp_mv'].pop()
            elif voltage == 'lv':
                subtype = subTypesListDct['hp_lv'].pop()
            
            while node in handledNodes or node in hpChp:
                node = random.choice(connectionNodes)
            handledNodes.append(node)
            hpChp.append(node)
            
        # Rule 4
        elif componentType == 'storage_mv' or componentType == 'storage_lv':

            componentTypeShort = 'Storage'
            #randSubType = random.choice(subTypesDct['storage'])
            #subtype = ','.join(randSubType)

            if voltage == 'mv':
                subtype = subTypesListDct['storage_mv'].pop()
            elif voltage == 'lv':
                subtype = subTypesListDct['storage_lv'].pop()

            # If additional Storage creation is allowed
            if additionalStorages:
                # First serve all nodes with producer components attached
                if StorageCounter < len(producerComponents):
                    while node in handledNodes or node not in producerComponents:
                        node = random.choice(connectionNodes)
                    handledNodes.append(node)
                # After all producer nodes are served, chose random, unhandled nodes
                else:
                    while node in handledNodes:
                        node = random.choice(connectionNodes)
                    handledNodes.append(node)
            # If no additional Storage creation allowed, simply attach a Storage to each producer node
            else:
                while node in handledNodes or node not in producerComponents:
                    node = random.choice(connectionNodes)
                handledNodes.append(node)
            # Counter needed for producer node preferability
            StorageCounter += 1

            
        # If component is neither CHP/HP nor Storage, no additional conditions for the node are needed...
        else:
            if componentType == 'load_mv' or componentType == 'load_lv':
                if voltage == 'mv':
                    componentTypeShort = 'Load'
                    subtype = subTypesListDct['load_mv'].pop()

                elif voltage == 'lv':
                    componentTypeShort = 'Load'
                    subtype = subTypesListDct['load_lv'].pop()

            elif componentType == 'pv_mv' or componentType == 'pv_lv':

                componentTypeShort = 'PV'

                if voltage == 'mv':
                    subtype = subTypesListDct['pv_mv'].pop()
                elif voltage == 'lv':
                    subtype = subTypesListDct['pv_lv'].pop()

            elif componentType == 'biofuel_mv' or componentType == 'biofuel_lv':

                componentTypeShort = 'Biofuel'

                if voltage == 'mv':
                    subtype = subTypesListDct['biofuel_mv'].pop()
                elif voltage == 'lv':
                    subtype = subTypesListDct['biofuel_lv'].pop()

            elif componentType == 'wind_mv' or componentType == 'wind_lv':

                componentTypeShort = 'Wind'

                if voltage == 'mv':
                    subtype = subTypesListDct['wind_mv'].pop()
                elif voltage == 'lv':
                    subtype = subTypesListDct['wind_lv'].pop()
            
            elif componentType == 'ev_mv' or componentType == 'ev_lv':

                componentTypeShort = 'EV'

                if voltage == 'mv':
                    subtype = subTypesListDct['ev_mv'].pop()
                elif voltage == 'lv':
                    subtype = subTypesListDct['ev_lv'].pop()
                
            elif componentType == 'compensator_mv' or componentType == 'compensator_lv':

                componentTypeShort = 'Compensator'

                if voltage == 'mv':
                    subtype = subTypesListDct['compensator_mv'].pop()
                elif voltage == 'lv':
                    subtype = subTypesListDct['compensator_lv'].pop()

            while node in handledNodes:
                node = random.choice(connectionNodes)
            handledNodes.append(node)

            # Rule 4!
            if componentTypeShort == 'PV' or componentTypeShort == 'Wind':
                if node not in producerComponents:
                    producerComponents.append(node)

        # For each created component, add the corresponding highestComponentId in the respective grid
        if voltage == 'mv' or voltage == 'lv' and onlyLV:
            scenariosList[0].highestComponentId += 1
            componentId = scenariosList[0].highestComponentId

        elif voltage == 'lv':
            scenariosList[2].highestComponentId += 1
            componentId = scenariosList[2].highestComponentId
        

        # The creation of the corresponding adding of the edge to the newScenario is done in the main script
        if voltage == 'mv':
            electricalgridsDct['elecgrid_mv'].append({'id1': node, 'id2': componentId, 'cabletype':'NO_LOSSES', 'arg1': 0,
                                                        'arg2': 0, 'arg3': 0, 'arg4': 0, 'arg5': 0, 'arg6': 0, 'arg7': 0})
            if any(communicationPercDct):
                if random.uniform(0.0,100.0) <= communicationPercDct['_'.join([componentTypeShort.lower(),voltage])]:
                    componentsDct['components_mv'][node-1][5] = '1'
                else:
                    componentsDct['components_mv'][node-1][5] = '0'
            

            # Set offset for medium voltage components, given when function was called
            componentOffset = componentOffsetMV

        # Low voltage components edges are simply added the added of the resulting newScenario el_grid dictionary
        elif voltage == 'lv':
            newScenarioElectricalGrid.append({'id1': node, 'id2': componentId, 'cabletype':'NO_LOSSES', 'arg1': 0,
                                                'arg2': 0, 'arg3': 0, 'arg4': 0, 'arg5': 0, 'arg6': 0, 'arg7': 0}) 

            if any(communicationPercDct):
                if random.uniform(0.0,100.0) <= communicationPercDct['_'.join([componentTypeShort.lower(),voltage])]:
                    componentsDct['components_lv'][node-1][5] = '1'
                else:
                    componentsDct['components_lv'][node-1][5] = '0'

            # LV voltage grids have no additional componentOffset, because they are created one after the other with the current highestComponentId + 1 set above
            componentOffset = 0
        

        # Create component and add it to the component dict of the newScenario
        newScenarioComponents.append((componentId + componentOffset, componentTypeShort, subtype))
        # Update actual nominal powers
        actualKWperComponentDct[componentType] += float(subtype[4])/1000

    # Delete the set of handled nodes, to be sure the next component type can be added to all nodes following the rules
    del handledNodes[:]

# Function to determine random subtypes for each component, based on the given nominal powers
def determineSubtypes(voltageLevel, wishedKWperComponentDct, actualKWperComponentDct, numberOfComponentsDct, subTypesDct, subTypesListsDct, subTypesPercDct):

    for key in wishedKWperComponentDct.keys():
        actualKWperComponentDct[key] = 0.0
        
        # If no percentages are given for subtype --> equal distribution 
        if key not in subTypesPercDct.keys():
            subTypesPercDct[key] = None

        if voltageLevel in key:
            #print(key)
            nominalPower = float(wishedKWperComponentDct[key])
            subTypesListsDct[key] = []
            numberOfComponentsDct[key] = 0
            # If user wishes to create components of the current type...
            if nominalPower != 0:
                # As long as there is still nominal power 'left'...
                while True:
                    # Chose random subtype from collection of possible subtypes...
                    randSubType = random.choices(subTypesDct[key],subTypesPercDct[key])[0]
                    #print(randSubType)
                    #print('%f - %f \n' % (nominalPower, (float(randSubType[4])/1000)))
                    # Substract nominal power of chosen subtype from the total nominal power wished
                    nominalPower -= float(randSubType[4])/1000

                    # If last chosen subtype perfectly fills the wished nominal power...
                    if nominalPower == 0:
                        # Add subtype as last subtype and break the loop
                        subTypesListsDct[key].append(randSubType)
                        numberOfComponentsDct[key] += 1
                        break

                    # If last chosen subtype has a bigger nominal power, than there is nominal power 'left' to distribute...
                    if nominalPower < 0:
                        #TODO More efficient if the biggest suitable subtype is chosen...
                        # Take subtype with smalles nominal power as last subtype and break loop...
                        subTypesListsDct[key].append(subTypesDct[key][0])
                        numberOfComponentsDct[key] += 1
                        break

                    subTypesListsDct[key].append(randSubType)
                    numberOfComponentsDct[key] += 1

# Function to determine random subtypes for each component, based on the given nominal powers
def determineSubtypesPerc(voltageLevel, wishedKWperComponentDct, actualKWperComponentDct, numberOfComponentsDct, subTypesDct, subTypesListsDct, subTypesPercDct):

    # For every component type, that should be created...
    for key in wishedKWperComponentDct.keys():
        actualKWperComponentDct[key] = 0.0
        
        # If no percentages are given for subtype --> equal distribution 
        if key not in subTypesPercDct.keys():
            subTypesPercDct[key] = [1/len(subTypesDct[key])]*len(subTypesDct[key])

        if voltageLevel in key:

            subTypesListsDct[key] = []
            numberOfComponentsDct[key] = 0

            
            for counter,subtype in enumerate(subTypesDct[key],0):

                # Only create the subtype if a nominal power as well as a percentage was given by the user
                if subTypesPercDct[key][counter] != 0.0 and wishedKWperComponentDct[key] != 0.0:

                    # numberOfSubtype = PnomOfComponent * percentageOfSubtype / PnomOfSubtype
                    numberOfSubtype = int(round(wishedKWperComponentDct[key] * subTypesPercDct[key][counter] / (float(subTypesDct[key][counter][4])/1000)))
                    # Create at least one!
                    if  numberOfSubtype < 1:
                        numberOfSubtype = 1
                else:
                    numberOfSubtype = 0

                for i in range(0,numberOfSubtype):
                    # Add subtype to list and increment component counter
                    subTypesListsDct[key].append(subTypesDct[key][counter])
                    numberOfComponentsDct[key] += 1