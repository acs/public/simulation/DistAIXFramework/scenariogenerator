###############################################################################
# Input reader module
#
# @author Sonja Happ <sonja.happ@eonerc.rwth-aachen.de>, Felix Wege
# @copyright 2019, Institute for Automation of Complex Power Systems, EONERC
# @license GNU General Public License (version 3)
#
# DistAIX Scenario Generator
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
###############################################################################

# This module is used to read in the user given amount of components for creation

# Medium voltage components
def readComponents(voltage, numberOfComponentsDct, connectionNodes, kWperComponentDct):

    # Convert function argument to string for correct prints
    if voltage == 'mv':
        voltageString = 'Medium voltage'
    elif voltage == 'lv':
        voltageString = 'Low voltage'
    # If voltage is undefined, give error message and return
    else:
        print('%s is no valid voltage level!!' % voltage)
        return

    print('Additional component information required...')
    print('There are %s free nodes available.' % len(connectionNodes))
    # If user wishes the creation of MV components, fill the corresponding dictionary
    if input('%s component creation wished? [y/n]' % voltageString) is 'y':

        # Additional components and portion of different subtypes for medium voltage grids
        if voltage == 'mv':

            numberOfComponentsDct['Biofuel'] = int(input('Biofuel:'))
            numberOfComponentsDct['Wind'] = int(input('Wind:'))
            
            print('Wished number of kilowatts per medium voltage component reqiured...')

            subTypes = ['Load', 'PV', 'CHP', 'HP', 'Compensator', 'Storage', 'Biofuel', 'Wind']
            for subType in subTypes:
                if numberOfComponentsDct[subType] > 0:
                    kWperComponentDct['_'.join([subType,'mv'])] = int(input('kW - %s: ' % subType))
            
        # Additional components for low voltage grids
        elif voltage == 'lv':

            numberOfComponentsDct['EV'] = int(input('EV:'))

            
            print('Wished number of kilowatts per low component reqiured...')

            subTypes = ['Load', 'PV', 'CHP', 'HP', 'Compensator', 'Storage', 'EV']
            for subType in subTypes:
                if numberOfComponentsDct[subType] > 0:
                    kWperComponentDct['_'.join([subType])] = int(input('kW - %s: ' % subType))
                    
    print('#####################\n')

# read nominal powers for each component
def readNominalPower(voltage, wishedKWperComponentDct):
    
    if voltage == 'mv':
        
        print('Wished number of kilowatts per medium voltage component reqiured...')

        subTypes = ['load', 'pv', 'chp', 'hp', 'compensator', 'biofuel', 'wind', 'storage']
        for subType in subTypes:
            while True:
                try:
                    wishedKWperComponentDct['_'.join([subType,'mv'])] = float(input('kW - %s: ' % subType))
                    break
                except ValueError:
                    print('No valid input - try again!')
        
    # Additional components for low voltage grids
    elif voltage == 'lv':
        
        print('Wished number of kilowatts per low component reqiured...')

        subTypes = ['load', 'pv', 'chp', 'hp', 'compensator', 'ev', 'wind', 'storage']
        for subType in subTypes:
            while True:
                try:
                    wishedKWperComponentDct['_'.join([subType,'lv'])] = float(input('kW - %s: ' % subType))
                    break
                except ValueError:
                    print('No valid input - try again!')
    
    # If voltage is undefined, give error message and return
    else:
        print('%s is no valid voltage level!!' % voltage)
        return

    print('#####################\n')

# read percentage with that a component type participates in communication
def readCommunicationPerc(voltage, communicationPercDct):
    if voltage == 'mv':

        print('Percentage values with which different component types participate in communication required...')
        subTypes = ['load', 'pv', 'chp', 'hp', 'compensator', 'biofuel', 'wind', 'storage']

        for subType in subTypes:
            while True:
                try:
                    communicationPercDct['_'.join([subType, 'mv'])] = float(input('Percentage - %s: ' % subType))
                    break
                except ValueError:
                    print('No valid input - try again!')


    elif voltage == 'lv':
        
        print('Percentage values with which different component types participate in communication required...')
        subTypes = ['load', 'pv', 'chp', 'hp', 'compensator', 'ev', 'wind', 'storage']

        for subType in subTypes:
            while True:
                try:
                    communicationPercDct['_'.join([subType, 'lv'])] = float(input('Percentage - %s: ' % subType))
                    break
                except ValueError:
                    print('No valid input - try again!')

    # If voltage is undefined, give error message and return                
    else:
        print('%s is no valid voltage level!!' % voltage)
        return

    print('#####################\n')


