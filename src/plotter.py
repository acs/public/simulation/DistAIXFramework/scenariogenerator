###############################################################################
# Plotter module
#
# @author Sonja Happ <sonja.happ@eonerc.rwth-aachen.de>, Felix Wege
# @copyright 2019, Institute for Automation of Complex Power Systems, EONERC
# @license GNU General Public License (version 3)
#
# DistAIX Scenario Generator
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
###############################################################################

# This module is used to plot the resulting grid after creation.
import pydot

def plot(scenariosList, newScenarioComponents, newScenarioElectricalGrid, newScenarioPath):
    # Create a graph
    graph = pydot.Dot(graph_type='graph')
    # For each element of the new Scenario
    for component in newScenarioComponents:
        # If element is a node type, i.e. Slack, Transformer or Node, plot as cricle
        if component[1] == 'Slack' or component[1] == 'Transformer' or component[1] == 'Node':
            # Create component
            node = pydot.Node(component[0], label = '\n'.join((str(component[0]),component[1])))
            # MV voltage nodes are plotted in blue, LV nodes in red.
            # If only a topology is created, scenariosList[0] will return an indexError --> print in green!
            try:
                highestMVId = scenariosList[0].highestNodeId
                if component[0] > highestMVId:
                    node.set_color('blue')
                else:
                    node.set_color('red')
                           
            except IndexError:
                # Only a topology was created... 
                node.set_color('green')
        # If element is a component in electrical grid terms (Loads, Producers, Concusmers, etc.), plot as rectangle
        else:
            node = pydot.Node(component[0], label = '\n'.join((str(component[0]),component[1])), shape = 'box')
            #node = pydot.Node(component['id'], label = '\n'.join((str(component['id']),component['type'],component['arg'])), shape = 'box')
        graph.add_node(node)

    # Create all edges
    for edge in newScenarioElectricalGrid:
        graph.add_edge(pydot.Edge(edge['id1'], edge['id2']))
    # Wrire resulting graph as png file
    graph.write_png('/'.join((newScenarioPath,'new_scenario.png')))